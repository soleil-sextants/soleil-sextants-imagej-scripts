
pathSave = "H:\\autoImagJ\\resultat\\";
pathRead = "H:\\autoImagJ\\sourceTIFF\\";



setBatchMode(true); 

// Display info about the files
  list = getFileList(pathRead);
  for (i=0; i<list.length; i++){
      print(list[i]+": "+File.length(pathRead+list[i])+"  "+File. dateLastModified(pathRead+list[i]));
      fileName = list[i]; 
      print("now opening stack"); open(pathRead+fileName); 
      width = getWidth;
      height = getHeight;
      depth = nSlices;

      xc = 916; yc = 1221;

//---------------------------------       

      if (xc<1024) {
                   size_x = xc; 
      } else {
             size_x = 2048-xc;
      } // end for if

//--------------------------------- 

      if (yc<1024) {
                   size_y = yc; 
      } else {
             size_y = 2048-yc;
      } // end for if

//--------------------------------- 

      if (size_x<size_y) {
                   size_selection = 2*size_x; 
      } else {
             size_selection = 2*size_y;
      } // end for if




      makeRectangle(xc-size_selection/2, yc-size_selection/2, size_selection ,size_selection);
      run("Crop");
      saveAs("Tiff", pathSave+"image_crop_centered_"+fileName); close();

      } // end of file

setBatchMode(false); 

print("***FINISHED***");



